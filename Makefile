
all: gnum4 mclog

cmacros: mclog

m4macros: gnum4 mclog

mpi-m4: gnum4-mpi mclog-mpi

gnum4:
	m4 mclog_m4.hpp.in  channels_defs.hpp  > mclog_m4.hpp

gnum4-mpi:
	m4 mpi_mclog_m4.hpp.in  channels_defs.hpp  > mpi_mclog_m4.hpp

mclog:
	g++ -std=c++11 -Wall -Wextra test.cpp

mclog-mpi:
	mpic++ -std=c++11 -Wall -Wextra test_mpi.cpp
