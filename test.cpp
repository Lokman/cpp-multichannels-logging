#include <iostream>
#include <sstream>

//#define LOG_GNUM4

#ifdef LOG_GNUM4
#include "mclog_m4.hpp"
#else
#include "mclog_cmacros.hpp"
LOG_NEW_CHANNEL(WORKING_ON)
#define LOG_WORKING_ON_DEBUG(MSG) \
  LOG_WORKING_ON_DEBUG_(MSG, __FILE__, __LINE__, __FUNCTION__)
#define LOG_WORKING_ON_INFO(MSG) \
  LOG_WORKING_ON_INFO_(MSG, __FILE__, __LINE__, __FUNCTION__)
#define LOG_WORKING_ON_ERROR(MSG) \
  LOG_WORKING_ON_ERROR_(MSG, __FILE__, __LINE__, __FUNCTION__)
LOG_NEW_CHANNEL(TEST_CATALYST)
#endif

using namespace std;

int main(void) {
  LOG_SET_MASK_WORKING_ON(LOG_ALL);
  
  stringstream msg;
  msg << "LOKMAN" << " Hi while LOGGing_ALL" ;
  LOG_WORKING_ON_INFO(msg.str());

#ifdef LOG_GNUM4
  LOG_WORKING_ON_INFO("Hi" << " using expression with stream op <<");
#endif

  LOG_WORKING_ON_DEBUG(msg.str());
  LOG_SET_MASK_WORKING_ON(LOG_INFO); 
  LOG_SET_COLOR_WORKING_ON(LOG_RED_TXT);
  msg << "LOKMAN" << " Hi while LOGGing_INFO only" ;
  LOG_WORKING_ON_INFO(msg.str());
  msg << "LOKMAN" << " Hi while LOGGing_INFO only" ;
  LOG_WORKING_ON_ERROR(msg.str());

#ifndef LOG_GNUM4
  LOG_TEST_CATALYST_INFO_("Hi with default logging config (LOG_NONE)", __FILE__, __LINE__, __FUNCTION__);
#endif  
  LOG_SET_MASK_TEST_CATALYST(LOG_ERROR); 
#ifndef LOG_GNUM4
  LOG_TEST_CATALYST_INFO_("Hi while LOGing_ERROR only", __FILE__, __LINE__, __FUNCTION__);
  LOG_TEST_CATALYST_ERROR_("Hi while LOGing_ERROR only", __FILE__, __LINE__, __FUNCTION__);
#endif
  return 0;
}
