#ifndef MULTICHANNEL_LOG_DEF
#define MULTICHANNEL_LOG_DEF

#include <iostream>
#include <string>
#include <sys/types.h>
#include <unistd.h>

using namespace std;

// Constants used for binary mask
//#define LOG_DEBUG 0x01
#define LOG_NONE 0
#define LOG_DEBUG 1
#define LOG_INFO 2
#define LOG_WARN 4 
#define LOG_ERROR 8
#define LOG_ALL LOG_DEBUG | LOG_INFO | LOG_WARN | LOG_ERROR

#define LOG_RED_TXT "31;1"
#define LOG_BLUE_TXT "34;1"
#define LOG_YELLOW_TXT "33;1"
#define LOG_GREEN_TXT "32;1"

#define LOG_RED_BKG "41;1"
#define LOG_BLUE_BKG "44;1"
#define LOG_YELLOW_BKG "43;1"
#define LOG_GREEN_BKG "42;1"

typedef struct LOG_CONFIG_STRUCT_ {
//  uint8_t mask = 0x00; 
  int mask = LOG_NONE;
  string color = LOG_GREEN_TXT;
} LOG_CONFIG_STRUCT_;


void LOG_FN_(string channel, string level, string msg, string file, int line, 
             string function, int level_mask, LOG_CONFIG_STRUCT_ config) {
  if (!(level_mask & config.mask))
    return;
  std::cerr << "\033[" << config.color << "m[" 
            << channel << " - " << level << "\t] "
            << "[" 
            << ::getpid() << ":" << "<rank>"
            << " "
            << file << ":"  << line << ":" << function
            << "]\033[0m " 
            << msg << std::endl; 
  std::cerr.flush();
}

#define MAKE_CHANNEL_FNS(x) \
  \
  LOG_CONFIG_STRUCT_ _ ## x ##_CONFIG;\
  void LOG_SET_MASK_ ## x (int mask) {_ ## x ##_CONFIG.mask = mask;}\
  \
  void LOG_SET_COLOR_ ## x (string color) {_ ## x ##_CONFIG.color = color;}\
  \
  void LOG_ ## x ##_DEBUG_(string msg, string file, int line, string function) { \
       LOG_FN_(#x ,"DEBUG", msg, file, line, function, LOG_DEBUG,  _ ## x ##_CONFIG); \
  } \
  void LOG_ ## x ##_INFO_(string msg, string file, int line, string function) { \
       LOG_FN_(#x ,"INFO", msg, file, line, function, LOG_INFO,  _ ## x ##_CONFIG); \
  } \
  void LOG_ ## x ##_WARN_(string msg, string file, int line, string function) { \
       LOG_FN_(#x ,"WARN", msg, file, line, function, LOG_WARN,  _ ## x ##_CONFIG); \
  } \
  void LOG_ ## x ##_ERROR_(string msg, string file, int line, string function) { \
       LOG_FN_(#x ,"ERROR", msg,  file, line, function, LOG_ERROR,  _ ## x ##_CONFIG); \
  } \

#define LOG_NEW_CHANNEL(channel) MAKE_CHANNEL_FNS(channel) 

#endif
