#ifndef MPI_MULTICHANNEL_LOG_DEF
#define MPI_MULTICHANNEL_LOG_DEF

#include <iostream>
#include <string>
#include <sys/types.h>
#include <unistd.h>
#include <mpi.h>

using namespace std;

// Constants used for binary mask
//#define LOG_DEBUG 0x01
#define LOG_NONE 0
#define LOG_DEBUG 1
#define LOG_INFO 2
#define LOG_WARN 4 
#define LOG_ERROR 8
#define LOG_ALL LOG_DEBUG | LOG_INFO | LOG_WARN | LOG_ERROR

#define LOG_RED_TXT "31;1"
#define LOG_BLUE_TXT "34;1"
#define LOG_YELLOW_TXT "33;1"
#define LOG_GREEN_TXT "32;1"

#define LOG_RED_BKG "41;1"
#define LOG_BLUE_BKG "44;1"
#define LOG_YELLOW_BKG "43;1"
#define LOG_GREEN_BKG "42;1"

typedef struct LOG_CONFIG_STRUCT_ {
//  uint8_t mask = 0x00; 
  int mask = LOG_NONE;
//  string color = "36;1";
  string color = LOG_GREEN_TXT;
} LOG_CONFIG_STRUCT_;

namespace utils {
namespace log {

inline int getrank()
{
	int mpiRank = -1; 
	MPI_Comm_rank(MPI_COMM_WORLD, &mpiRank); 
	return mpiRank;
}

} // utils namespace
} // log namespace


#define LOG_(CHANNEL, LEVEL, MSG) \
	if (utils::log::getrank() == 0) { \
	if ( _ ## CHANNEL ##_CONFIG.mask & LOG_ ## LEVEL) { \
  std::cerr << "\033[" << _ ## CHANNEL ##_CONFIG.color << "m[" \
            << #CHANNEL << " - " << #LEVEL << "\t] " \
            << "["  \
            << ::getpid() << ":" << "<rank>" \
            << " " \
            << __FILE__ << ":"  << __LINE__ << ":" << __FUNCTION__ \
            << "]\033[0m " \
            << MSG << std::endl; \
  std::cerr.flush(); \
	}}



#endif


LOG_CONFIG_STRUCT_ _WORKING_ON_CONFIG;
#define LOG_SET_MASK_WORKING_ON(MASK) \
	_WORKING_ON_CONFIG.mask = MASK;
#define LOG_SET_COLOR_WORKING_ON(COLOR) \
	_WORKING_ON_CONFIG.color = COLOR;
#define LOG_SET_OSTREAM_WORKING_ON(STREAM) \
	do {} while(0);

#define LOG_WORKING_ON_DEBUG(MSG) \
  LOG_(WORKING_ON, DEBUG, MSG)
#define LOG_WORKING_ON_INFO(MSG) \
  LOG_(WORKING_ON, INFO, MSG)
#define LOG_WORKING_ON_WARN(MSG) \
  LOG_(WORKING_ON, WARN, MSG)
#define LOG_WORKING_ON_ERROR(MSG) \
  LOG_(WORKING_ON, ERROR, MSG)	

LOG_CONFIG_STRUCT_ _TEST_CATALYST_CONFIG;
#define LOG_SET_MASK_TEST_CATALYST(MASK) \
	_TEST_CATALYST_CONFIG.mask = MASK;
#define LOG_SET_COLOR_TEST_CATALYST(COLOR) \
	_TEST_CATALYST_CONFIG.color = COLOR;
#define LOG_SET_OSTREAM_TEST_CATALYST(STREAM) \
	do {} while(0);

#define LOG_TEST_CATALYST_DEBUG(MSG) \
  LOG_(TEST_CATALYST, DEBUG, MSG)
#define LOG_TEST_CATALYST_INFO(MSG) \
  LOG_(TEST_CATALYST, INFO, MSG)
#define LOG_TEST_CATALYST_WARN(MSG) \
  LOG_(TEST_CATALYST, WARN, MSG)
#define LOG_TEST_CATALYST_ERROR(MSG) \
  LOG_(TEST_CATALYST, ERROR, MSG)	

