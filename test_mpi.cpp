#include <iostream>
#include <sstream>

#include "mpi_mclog_m4.hpp"

using namespace std;

int main(int argc, char **argv) {
  MPI_Init(&argc,&argv);

  LOG_SET_MASK_WORKING_ON(LOG_ALL);
   
  LOG_WORKING_ON_INFO("LOKMAN" << " Hi while LOGGing_ALL");
  
  LOG_WORKING_ON_DEBUG("LOKMAN" << " Hi while LOGGing_ALL");
  LOG_SET_MASK_WORKING_ON(LOG_INFO); 
  LOG_SET_COLOR_WORKING_ON(LOG_RED_TXT);
  
  LOG_WORKING_ON_INFO("LOKMAN" << " Hi while LOGGing_INFO only");
  
  LOG_WORKING_ON_ERROR("LOKMAN" << " Hi while LOGGing_INFO only");

  LOG_SET_MASK_TEST_CATALYST(LOG_ERROR); 
  return 0;
}
