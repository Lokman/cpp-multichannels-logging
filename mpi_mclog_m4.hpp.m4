#ifndef MPI_MULTICHANNEL_LOG_DEF
#define MPI_MULTICHANNEL_LOG_DEF

#include <iostream>
#include <fstream>
#include <ostream>
#include <string>
#include <unordered_map>
#include <list>
#include <getopt.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <ctime>
#include <cstring>
#include <mpi.h>

using namespace std;

// Constants used for binary mask
// TAG: MANUALLY
//#define LOG_DEBUG 0x01
#define LOG_NONE 0
#define LOG_DEBUG 1
#define LOG_INFO 2
#define LOG_WARN 4 
#define LOG_ERROR 8
#define LOG_ALL LOG_DEBUG | LOG_INFO | LOG_WARN | LOG_ERROR

#define LOG_RED_TXT "31;1"
#define LOG_BLUE_TXT "34;1"
#define LOG_GREEN_TXT "32;1"
#define LOG_YELLOW_TXT "33;1"
#define LOG_WHITE_TXT "30;1" // TOFIX Grey not white ..
#define LOG_RED_BKG "41;1"
#define LOG_BLUE_BKG "44;1"
#define LOG_YELLOW_BKG "43;1"
#define LOG_GREEN_BKG "42;1"

#define LOG_STREAM_COUT 'O'
#define LOG_STREAM_CERR 'E'
#define LOG_STREAM_FILE 'F'
#define LOG_STREAM_MEM  'M'

#define LOG_RANK_ALL -42

//using namespace std;

typedef struct LOG_CONFIG_STRUCT_ {
//  uint8_t mask = 0x00; 
  int mask = LOG_ALL;
  string color = LOG_WHITE_TXT;
  int rank = 0;
  ostream* stream = &(std::cerr);

} LOG_CONFIG_STRUCT_;

typedef struct TIMER_CONFIG_ {
  std::unordered_map <std::string,double> list;
  std::ostream *stream = nullptr;
} TIMER_CONFIG_;

namespace Utils {
namespace Log {

inline int getrank()
{
	int mpiRank = -1; 
	MPI_Comm_rank(MPI_COMM_WORLD, &mpiRank); 
	return mpiRank;
}

inline int getsize()
{
	int mpiSize = -1; 
	MPI_Comm_size(MPI_COMM_WORLD, &mpiSize); 
	return mpiSize;
}

inline int getrunid(int t)
{
  static int run_id = -1;
  
  if (run_id == -1){
    run_id = t;
  }

  return run_id;
}

int parse_options_(int *pargc, char ***pargv);

} // Log namespace

namespace Mem {

inline unsigned long GetCurrentKB()
{
  std::ifstream fin("/proc/self/status");
  std::string line;                
  unsigned long memUsage = 0;
  string memUsageS = "";
 
  char phymem[] = "VmRSS:";
  while (std::getline(fin, line, '\n')){
    //if (Utils::Log::getrank() == 0) cerr << "status line: " << line << endl;
    if(strncmp(line.c_str(), phymem, 6) == 0){
      for (int i=0 ; i<line.size(); i++){
        if ((line[i] >= '0') && (line[i] <= '9'))
          memUsageS += line[i];
      }
      break;
    }
  }
  
  memUsage = std::stoul(memUsageS);
  return memUsage;
}

} // Mem namespace

namespace Timer {

extern TIMER_CONFIG_ timers;

} // Timer namespace


} // Utils namespace


#define LOG_(CHANNEL, LEVEL, MSG, ALL_RANKS) \
	if ( ALL_RANKS \
       || Utils::Log::getrank() == Utils::Log::_ ## CHANNEL ##_CONFIG.rank \
       || Utils::Log::_ ## CHANNEL ##_CONFIG.rank == LOG_RANK_ALL) { \
	if ( Utils::Log::_ ## CHANNEL ##_CONFIG.mask & LOG_ ## LEVEL) { \
  *(Utils::Log::_ ## CHANNEL ##_CONFIG.stream) << \
            "\033[" << Utils::Log::_ ## CHANNEL ##_CONFIG.color << "m[" \
            << #CHANNEL << " - " << #LEVEL << "\t] " \
            << "["  \
            << ::getpid() << ":" << Utils::Log::getrank() \
            << " " \
            << __FILE__ << ":"  << __LINE__ << ":" << __FUNCTION__ \
            << "]\033[0m " \
            << MSG << std::endl; \
  std::cerr.flush(); \
	}}

////TOFIX Redundent
//#define LOG_ALL_(CHANNEL, LEVEL, MSG) \
//	if (Utils::Log::getrank() == Utils::Log::_ ## CHANNEL ##_CONFIG.rank) { \
//	if ( Utils::Log::_ ## CHANNEL ##_CONFIG.mask & LOG_ ## LEVEL) { \
//  *(Utils::Log::_ ## CHANNEL ##_CONFIG.stream) << \
//            "\033[" << Utils::Log::_ ## CHANNEL ##_CONFIG.color << "m[" \
//            << #CHANNEL << " - " << #LEVEL << "\t] " \
//            << "["  \
//            << ::getpid() << ":" << Utils::Log::getrank() \
//            << " " \
//            << __FILE__ << ":"  << __LINE__ << ":" << __FUNCTION__ \
//            << "]\033[0m " \
//            << MSG << std::endl; \
//  std::cerr.flush(); \
//	}}

#define LOG_SET_RUN_ID(comm) {\
  int t = -1; \
  if (Utils::Log::getrank() == 0){ \
    t = time(NULL); \
  } \
  MPI_Bcast((void*) &t, 1, MPI_INT, 0, comm); \
  Utils::Log::getrunid(t); }



#define TIMER_SET(NAME) \
  Utils::Timer::timers.list[NAME] = MPI_Wtime();

#define TIMER_INIT() \
 namespace Utils { \
 namespace Timer { \
  TIMER_CONFIG_ timers;\
 } \
 }

#define TIMER_GET(NAME, PTIME) {\
  double _mpi_wtime_finish = MPI_Wtime(); \
	std::unordered_map <std::string, double>::const_iterator _result_find = \
    Utils::Timer::timers.list.find(NAME); \
	if (_result_find != Utils::Timer::timers.list.end()) \
		*PTIME = _mpi_wtime_finish - _result_find->second; \
	else \
    *PTIME = -1; }

// TOFIX for LOG_STREAM_FILE there is one extra if for each call
#define TIMER_GET_LOG(NAME, PTIME, STREAM, EXTRA_MSG) \
  TIMER_GET(NAME, PTIME); \
  if(STREAM == LOG_STREAM_COUT) \
    std::cout << "[TIMER]\t" << Utils::Log::getrank() << "\t" \
    << NAME << "\t" << *PTIME << "\t" << EXTRA_MSG << std::endl; \
  if(STREAM == LOG_STREAM_CERR) \
    std::cout << "[TIMER]\t" << Utils::Log::getrank() << "\t" \
    << NAME << "\t" << *PTIME << "\t" << EXTRA_MSG << std::endl; \
  if(STREAM == LOG_STREAM_FILE){ \
    if (Utils::Timer::timers.stream == nullptr) \
      Utils::Timer::timers.stream = \
        new std::ofstream("logs/timer-" + \
        std::to_string(Utils::Log::getsize()) + "-" + \
        std::to_string(Utils::Log::getrunid(0)) + "-" + \
        std::to_string(Utils::Log::getrank()), std::ofstream::out); \
    *(Utils::Timer::timers.stream) << "[TIMER]\t" << Utils::Log::getrank() \
      << "\t" << NAME << "\t" << *PTIME << "\t" << EXTRA_MSG << std::endl; \
    Utils::Timer::timers.stream->flush(); } \

// TOFIX redundent code
#define TIMER_GET_LOG_RANK(NAME, PTIME, STREAM, RANK) \
  TIMER_GET(NAME, PTIME); \
  if (Utils::Log::getrank() == RANK) {\
  if(STREAM == LOG_STREAM_COUT) \
    std::cout << "[TIMER]\t" << Utils::Log::getrank() << "\t" \
    << NAME << "\t" << *PTIME << std::endl; \
  if(STREAM == LOG_STREAM_CERR) \
    std::cout << "[TIMER]\t" << Utils::Log::getrank() << "\t" \
    << NAME << "\t" << *PTIME << std::endl; \
  if(STREAM == LOG_STREAM_FILE){ \
    if (Utils::Timer::timers.stream == nullptr) \
      Utils::Timer::timers.stream = \
        new std::ofstream("logs/timer-" + \
        std::to_string(Utils::Log::getsize()) + "-" + \
        std::to_string(Utils::Log::getrunid(0)) + "-" + \
        std::to_string(Utils::Log::getrank()), std::ofstream::out); \
    *(Utils::Timer::timers.stream) << "[TIMER]\t" << Utils::Log::getrank() \
      << "\t" << NAME << "\t" << *PTIME << std::endl; \
    Utils::Timer::timers.stream->flush(); } \
  }\
      
#define TIMER_SET_ALL(MPICOMM, NAME) \
  MPI_Barrier(MPICOMM); \
  TIMER_SET(NAME);

// TOFIX need to store MPI COMM at SET time
#define TIMER_GET_ALL(MPICOMM, NAME, PTIME) \
  MPI_Barrier(MPICOMM); \
  TIMER_GET(NAME, PTIME);

#define TIMER_GET_ALL_LOG(MPICOMM, NAME, PTIME, STREAM, RANK) \
  TIMER_GET_ALL(MPICOMM, NAME, PTIME); \
  if (Utils::Log::getrank() == RANK) {\
  if(STREAM == LOG_STREAM_COUT) \
    std::cout << "[TIMER]\tALL\t" \
    << NAME << "\t" << *PTIME << std::endl; \
  if(STREAM == LOG_STREAM_CERR) \
    std::cout << "[TIMER]\tALL\t" \
    << NAME << "\t" << *PTIME << std::endl; \
  if(STREAM == LOG_STREAM_FILE){ \
    if (Utils::Timer::timers.stream == nullptr) \
      Utils::Timer::timers.stream = \
        new std::ofstream("logs/timer-" + "-" + \
        std::to_string(Utils::Log::getsize()) + "-" + \
        std::to_string(Utils::Log::getrunid(0)) + "-" + \
        std::to_string(Utils::Log::getrank()), std::ofstream::out); \
    *(Utils::Timer::timers.stream) << "[TIMER]\tALL\t" \
      << NAME << "\t" << *PTIME << std::endl; \
    Utils::Timer::timers.stream->flush(); } \
  }\
  
#define TIMER_GET_MINE(NAME, PTIME, EXTRA_MSG)\
  TIMER_GET_LOG(NAME, PTIME, LOG_STREAM_FILE, EXTRA_MSG)

#define LOG_RANK_ONLY(RANK,STREAM, MSG) \
  if (Utils::Log::getrank() == RANK) \
    STREAM << MSG << std::endl;

define(LOG_NEW_CHANNEL,`

namespace Utils {
namespace Log {
extern  LOG_CONFIG_STRUCT_ _$1_CONFIG;
}
}

divert(`1')dnl
  LOG_CONFIG_STRUCT_ _$1_CONFIG; \
divert 

// TAG: MANUALLY
divert(`2')dnl
  Utils::Log::int_config["$1.mask"]=&(Utils::Log::_$1_CONFIG.mask); \
  Utils::Log::str_config["$1.color"]=&(Utils::Log::_$1_CONFIG.color); \
  Utils::Log::int_config["$1.rank"]=&(Utils::Log::_$1_CONFIG.rank); \
  Utils::Log::stream_config["$1.stream"]=&(Utils::Log::_$1_CONFIG.stream); \
  Utils::Log::all_channels.push_back("$1"); \
divert

#define LOG_SET_LEVEL_$1(MASK) \
  Utils::Log::_$1_CONFIG.mask = MASK;
#define LOG_SET_COLOR_$1(COLOR) \
	Utils::Log::_$1_CONFIG.color = COLOR;
//TOFIX test for file stream open error
#define LOG_SET_OSTRM_$1(STREAM) \
	if (STREAM == LOG_STREAM_COUT) \
	  Utils::Log::_$1_CONFIG.stream = &(std::cout); \
  if (STREAM == LOG_STREAM_CERR) \
	  Utils::Log::_$1_CONFIG.stream = &(std::cerr); \
  if (STREAM == LOG_STREAM_FILE) \
	  if (Utils::Log::getrank() == Utils::Log::_$1_CONFIG.rank || \
        Utils::Log::_$1_CONFIG.rank == LOG_RANK_ALL) \
    Utils::Log::_$1_CONFIG.stream = \
    new std::ofstream("logs/log-`$1'-" + \
    std::to_string(Utils::Log::getrank()), std::ofstream::out); \
  if (STREAM == LOG_STREAM_MEM) \
    Utils::Log::_$1_CONFIG.stream = new std::ostringstream();
#define LOG_SET_LRANK_$1(RANK) \
  Utils::Log::_$1_CONFIG.rank = RANK;
    
    

#define LOG_$1_DEBUG(MSG) \
  LOG_($1, DEBUG, MSG, 0)
#define LOG_$1_INFO(MSG) \
  LOG_($1, INFO, MSG, 0)
#define LOG_$1_WARN(MSG) \
  LOG_($1, WARN, MSG, 0)
#define LOG_$1_ERROR(MSG) \
  LOG_($1, ERROR, MSG, 0)	
')

include(channels_defs.hpp)

#define LOG_INIT() \
  namespace Utils { \
  namespace Log { \
  undivert(`1')dnl  \
  std::unordered_map <std::string,int*> int_config; \
  std::unordered_map <std::string,std::string*> str_config; \
  std::unordered_map <std::string,std::ostream**> stream_config; \
  std::unordered_map <std::string,int> int_config_const; \
  std::unordered_map <std::string,std::string> str_config_const; \
  std::unordered_map <std::string,std::ostream*> stream_config_const; \
  std::list<string> all_channels; \
  } \
  }

//#define LOG_PARSE_OPTIONS(PARGC,PARGV,MAP) \
//  Utils::Log::parse_options_(PARGC,PARGV,MAP);

// TAG: MANUALLY
#define LOG_PARSE_OPTIONS(PARGC,PARGV) \
  undivert(`2')dnl \
  Utils::Log::int_config_const["NONE"] = LOG_NONE;\
  Utils::Log::int_config_const["DEBUG"] = LOG_DEBUG | LOG_INFO | LOG_WARN | LOG_ERROR;\
  Utils::Log::int_config_const["INFO"] = LOG_INFO | LOG_WARN | LOG_ERROR;\
  Utils::Log::int_config_const["WARN"] = LOG_WARN | LOG_ERROR;\
  Utils::Log::int_config_const["ERROR"] = LOG_ERROR;\
  Utils::Log::str_config_const["WHITE_TXT"] = "30;1";\
  Utils::Log::str_config_const["RED_TXT"] = "31;1";\
  Utils::Log::str_config_const["GREEN_TXT"] = "32;1";\
  Utils::Log::str_config_const["YELLOW_TXT"] = "33;1";\
  Utils::Log::str_config_const["BLUE_TXT"] = "34;1";\
  Utils::Log::str_config_const["RED_BKG"] = "41;1";\
  Utils::Log::str_config_const["GREEN_BKG"] = "42;1";\
  Utils::Log::str_config_const["YELLOW_BKG"] = "43;1";\
  Utils::Log::str_config_const["BLUE_BKG"] = "44;1";\
  Utils::Log::stream_config_const["STREAM_COUT"] = &(std::cout);\
  Utils::Log::stream_config_const["STREAM_CERR"] = &(std::cerr);\
  Utils::Log::stream_config_const["STREAM_FILE"] = &(std::cerr);\
  Utils::Log::parse_options_(PARGC,PARGV);


#endif

