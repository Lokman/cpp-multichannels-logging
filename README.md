mpi-multichannel-logging offers a simple and efficient multi-channel hierarchical logging library for 
MPI projects written in C/C++.

### simple

The developer have just to call `LOG_NEW_CHANNEL()` GNU m4 macros to generate all
needed functions ready to use to log into the created channel and eventually configure it.

Possible configurations for each channel:
- Logging stream: memory, stdout, stderr, file
- Logging level: debug, info, warn, error, critical
- Logging MPI rank process
- Logging text and background color

All configuration parameters can be set at run time as a short or long arguments
next to the executable of the code using the library.

### efficient

All logging and configuration functions are implemented as macros or inline functions.
This means that there will be no (i) function call overhead (ii) push/pop variables
on the stack overhead and (ii) return from call overhead.

If the a logging channel is disabled all correponding functions will have zero
effect on the code using the library as they will resolve to empty lines.

## Named MPI Timers
This library proposes also a named timer functions implemented as a special logging
channel. For more details check [dedicated wikipage](https://gitlab.com/Lokman/cpp-multichannels-logging/wikis/mpi-named-timers).

## Wiki
Technical [wiki](https://gitlab.com/Lokman/cpp-multichannels-logging/wikis/home).
