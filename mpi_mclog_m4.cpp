#include "mpi_mclog_m4.hpp"

using namespace std;

//inline int Utils::Log::getrank()
//{
//	int mpiRank = -1; 
//	MPI_Comm_rank(MPI_COMM_WORLD, &mpiRank); 
//	return mpiRank;
//}

namespace Utils {
namespace Log {

extern std::unordered_map <std::string,int*> int_config; 
extern std::unordered_map <std::string,std::string*> str_config; 
extern std::unordered_map <std::string,std::ostream**> stream_config; 
extern std::unordered_map <std::string,int> int_config_const; 
extern std::unordered_map <std::string,std::string> str_config_const; 
extern std::unordered_map <std::string,std::ostream*> stream_config_const; 
extern std::list<std::string> all_channels;
}


int Utils::Log::parse_options_(int *pargc, char ***pargv)
{
  char **argv = *pargv;
  int argc = *pargc;

  int opt_char;
  char *myconfig = NULL;

  struct option longopts[] = {
    {"log", required_argument, NULL, 'l'},
    {0,0,0,0}
  };

  while ((opt_char = getopt_long(argc,argv, ":l:", longopts, NULL)) != -1) {
    switch (opt_char) {
    case 'l':
      myconfig = optarg;
      break;
    case 0:
      break;
    case ':':
      std::cerr << argv[0] << ": option -" 
        << (char)optopt << " requires an argument" << std::endl;
      break;
    case '?':
    default:
      std::cerr << argv[0] << ": option -" 
        << (char)optopt << " is invalid: ignored" << std::endl;
      break;
    }
  }
  
  std::string channel="", field="", value="";
  // parse myconfig
  if (myconfig != NULL){
    //LOG_RANK_ONLY(0, std::cout, "============= " << myconfig);
    std::string sconfig(myconfig);
    int stage = 0;
    for (int i=0; i<sconfig.size() ; i++){
      // TODO need to check for errors!!!!!!!!!
      if (sconfig[i] == ',') {
        std::string this_config(channel+"."+field);
        LOG_RANK_ONLY(0, std::cout, 
          " i " << i << " ============ " << this_config << " value " 
          << value);
        //   configs[this_config] = std::stoi(value);
        // TAG: MANUALLY
        if(std::strcmp(channel.c_str(), "ALL") == 0)
        {
          for (std::string chnl : all_channels) {
            if(std::strcmp(field.c_str(), "mask") == 0)
              *(Utils::Log::int_config[chnl+"."+field]) = Utils::Log::int_config_const[value];
            if(std::strcmp(field.c_str(), "rank") == 0)
              *(Utils::Log::int_config[chnl+"."+field]) = std::stoi(value);
            if(std::strcmp(field.c_str(), "color") == 0)
              *(Utils::Log::str_config[chnl+"."+field]) = Utils::Log::str_config_const[value];
            if(std::strcmp(field.c_str(), "stream") == 0)
              *(Utils::Log::stream_config[chnl+"."+field]) = Utils::Log::stream_config_const[value];
          }
        } 
        else 
        {
          if(std::strcmp(field.c_str(), "mask") == 0)
            *(Utils::Log::int_config[channel+"."+field]) = Utils::Log::int_config_const[value];
          if(std::strcmp(field.c_str(), "rank") == 0)
            *(Utils::Log::int_config[channel+"."+field]) = std::stoi(value);
          if(std::strcmp(field.c_str(), "color") == 0)
            *(Utils::Log::str_config[channel+"."+field]) = Utils::Log::str_config_const[value];
          if(std::strcmp(field.c_str(), "stream") == 0)
            *(Utils::Log::stream_config[channel+"."+field]) = Utils::Log::stream_config_const[value];
        }
          
        
        stage=0;
        channel="";
        field="";
        value="";
        continue;
      }
      if (sconfig[i] == '.')
        stage++;
      else {
        switch (stage) {
        case 0: 
          channel += sconfig[i];
          break;
        case 1:
          field += sconfig[i];
          break;
        case 2:
          value += sconfig[i];
          break;
        default:
          break;
        }
        
      }
    }

    // Add the last config
    std::string this_config(channel+"."+field);
    LOG_RANK_ONLY(0, std::cout, 
      " i last " << " ============ " << this_config << " value " 
      << value);
    //configs[this_config] = std::stoi(value);
  
    // TAG: MANUALLY
    if(std::strcmp(channel.c_str(), "ALL") == 0)
    {
      for (std::string chnl : all_channels) {
        if(std::strcmp(field.c_str(), "mask") == 0)
          *(Utils::Log::int_config[chnl+"."+field]) = Utils::Log::int_config_const[value];
        if(std::strcmp(field.c_str(), "rank") == 0)
          *(Utils::Log::int_config[chnl+"."+field]) = std::stoi(value);
        if(std::strcmp(field.c_str(), "color") == 0)
          *(Utils::Log::str_config[chnl+"."+field]) = Utils::Log::str_config_const[value];
        if(std::strcmp(field.c_str(), "stream") == 0)
          *(Utils::Log::stream_config[chnl+"."+field]) = Utils::Log::stream_config_const[value];
      }
    } 
    else 
    {
      if(std::strcmp(field.c_str(), "mask") == 0)
        *(Utils::Log::int_config[channel+"."+field]) = Utils::Log::int_config_const[value];
      if(std::strcmp(field.c_str(), "rank") == 0)
        *(Utils::Log::int_config[channel+"."+field]) = std::stoi(value);
      if(std::strcmp(field.c_str(), "color") == 0)
        *(Utils::Log::str_config[channel+"."+field]) = Utils::Log::str_config_const[value];
      if(std::strcmp(field.c_str(), "stream") == 0)
        *(Utils::Log::stream_config[channel+"."+field]) = Utils::Log::stream_config_const[value];
    }

    LOG_RANK_ONLY(0, std::cout, "=========== myconfig " << myconfig);
//    std::cout << "Channel " << channel << " - field " << field << " - value "
//      << value << std::endl;
  
  for (int i=1; i<argc ; i++){
    argv[i] = argv[i+optind-1];
  }
  *pargc = (argc-optind+1);

//  i = 0;
//  while (i < argc)
//  {
//    i++;
//  }
  }
}
}
//inline unsigned long Utils::Mem::GetCurrentKB()
//{
//  std::ifstream fin("/proc/self/status");
//  std::string line;                
//  unsigned long memUsage = 0;
//  string memUsageS = "";
// 
//  char phymem[] = "VmRSS:";
//  while (std::getline(fin, line, '\n')){
//    //if (Utils::Log::getrank() == 0) cerr << "status line: " << line << endl;
//    if(strncmp(line.c_str(), phymem, 6) == 0){
//      for (int i=0 ; i<line.size(); i++){
//        if ((line[i] >= '0') && (line[i] <= '9'))
//          memUsageS += line[i];
//      }
//      break;
//    }
//  }
//  
//  memUsage = std::stoul(memUsageS);
//  return memUsage;
//}



